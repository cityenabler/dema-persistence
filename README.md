# IoTDeviceManager Persistence Layer

Spring Boot project - Microservice for IoTDeviceManager Persistence Layer

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning
We use [SemVer](http://semver.org/) for versioning.
