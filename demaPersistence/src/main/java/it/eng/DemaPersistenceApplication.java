package it.eng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemaPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemaPersistenceApplication.class, args);
	}

}
